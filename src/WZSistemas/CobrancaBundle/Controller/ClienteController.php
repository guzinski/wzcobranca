<?php

namespace WZSistemas\CobrancaBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use WZSistemas\CobrancaBundle\Entity\Cartao;
use WZSistemas\CobrancaBundle\Entity\Cliente;
use WZSistemas\CobrancaBundle\Form\ClienteType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Exception\Exception;
use ZipArchive;

/**
 * Description of ClienteController
 *
 * @author Luciano
 */
class ClienteController extends Controller
{
    /**
     * @Route("/clientes", name="clientes")
     * @Template()
     */
    public function indexAction() 
    {
        return array();
    }
    
    /**
     * @Route("/clientes/pagination", name="clientes_pagination")
     * @Template()
     */
    public function paginationAction(Request $request)
    {
        $firstResult    = $request->query->getInt("start");
        $maxResults     = $request->query->getInt("length");
        $busca          = $request->get("search");
        $ordem          = $request->get("order");
        
        $repClientes = $this->getDoctrine()
                            ->getRepository("WZSistemas\CobrancaBundle\Entity\Cliente");
        $clientes = $repClientes->getClientes($busca['value'], $maxResults, $firstResult, $ordem);
        
        $dados = array();
        foreach ($clientes as $cliente) {
            $link = $this->generateUrl("clientes_cadastro", array("id"=>$cliente->getId()));
            $dados[] = [
                "<a href=\"".$link."\">". $cliente->getNome()."</a>",
                $cliente->getTelefone(),
                $cliente->getNascimento()->format('d/m'),
            ];
        }
        $return = [
            'draw' => $request->get("draw"),
            'recordsTotal' => $repClientes->count(),
            'recordsFiltered' => $repClientes->count($busca['value']),
            'data' => $dados,
        ];
        return new Response(json_encode($return));
    }
 
    /**
     * @Route("/cliente/cadastro/{id}", name="clientes_cadastro")
     * @Template()
     * 
     * @param Request $request
     * @return RedirectResponse
     */
    public function cadastroAction(Request $request, $id=0)
    {
        $em = $this->getDoctrine()->getManager();
        if ($id>0) {
            $cliente = $em->find("WZSistemas\CobrancaBundle\Entity\Cliente", $id);
        } else {
            $cliente = new Cliente();
        }

        $arquivos = clone $cliente->getArquivos();
        $dependentes = clone $cliente->getDependentes();
        $form = $this->createForm(new ClienteType(), $cliente, array('em'=>$em));
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->get("cliente")->salvarCliente($cliente, $arquivos, $dependentes);                        
            return new RedirectResponse($this->generateUrl('clientes'));
        }
        
        return ['form'=>$form->createView(), 'cliente'=>$cliente];
    }
    
    /**
     * @Route("/cadastro/amador/{id}", name="cadastro_amador")
     * @Template()
     * 
     * @param Request $request
     * @return RedirectResponse
     */
    public function amadorAction(Request $request, $id=0)
    {
        return $this->cadastro($request, $id, "Amador");
    }
    
    /**
     * @Route("/cadastro/profissional/{id}", name="cadastro_profissional")
     * @Template()
     * 
     * @param Request $request
     * @return RedirectResponse
     */
    public function profissionalAction(Request $request, $id=0)
    {
        return $this->cadastro($request, $id, "Profissional");
    }
    
    
    private function cadastro(Request $request, $id=0, $tipo) 
    {
        $em = $this->getDoctrine()->getManager();
        if ($id>0) {
            $cliente = $em->find("WZSistemas\CobrancaBundle\Entity\Cliente", $id);
        } else {
            $cliente = new Cliente();
        }
        $logger = $this->get('logger');
        $logger->error('Total de dependebtes' + $cliente->getDependentes()->count());

        $arquivos = clone $cliente->getArquivos();
        $dependentes = clone $cliente->getDependentes();
        $form = $this->createForm(new ClienteType(), $cliente, array('em'=>$em));
        $form->handleRequest($request);
        if ($form->isValid()) {
            $cliente->setTipo(strtoupper($tipo));
            $this->get("cliente")->salvarCliente($cliente, $arquivos, $dependentes);                        
            return new RedirectResponse($this->generateUrl('clientes'));
        }
        
        return ['form'=>$form->createView(), 'cliente'=>$cliente, 'tipo'=>$tipo];
    }

    /**
     * @Route("/cliente/arquivos", name="cliente_arquivos")
     * 
     * @Template
     * @param int $idParcela
     * @return Response
     */
    public function listaDocumentosAction(Request $request)
    {
        $id = $request->get("id", null);
        if ($id==null) {
            throw new Exception("Cliente não foi encontrado");
        }

        $cliente = $this->getDoctrine()->getRepository("WZSistemas\\CobrancaBundle\\Entity\\Cliente")->find($id);
        if ($cliente==null) {
            throw new Exception("Cliente não foi encontrado");
        }
        return array("cliente"=>$cliente);
    }
    
    /**
     * @Route("/cliente/download/{nome}", name="cliente_arquivo_download")
     * 
     * @param int $nome Nome do Arquivo
     * @return Response
     */
    public function downloadArquivoAction($nome = null)
    {
        if (is_null($nome)) {
            throw new NotFoundHttpException;
        }
        if (file_exists($this->get('kernel')->getRootDir() . '/../web/uploads/arquivos/'.$nome)) {
            return $this->get("upload")->download($this->get('kernel')->getRootDir() . '/../web/uploads/arquivos/'.$nome);
        } elseif (file_exists($this->get('kernel')->getRootDir() . '/../web/uploads/temp/'.$nome)) {
            return $this->get("upload")->download($this->get('kernel')->getRootDir() . '/../web/uploads/temp/'.$nome);
        } else {
            throw new NotFoundHttpException;
        }
    }
    
    /**
     * @Route("/cliente/carta/aniversario/{nome}", name="cliente_carta")
     * 
     * @Template
     * @param int $idParcela
     * @return Response
     */
    public function cartaAction($nome)
    {
         return array("clienteNome"=> $nome);
    }
    
    
    /**
     * @Route("/cliente/gerar/cartao", name="cliente_gerar_cartao")
     * 
     * @return Response
     */
    public function gerarCartoes()
    {
        $em = $this->getDoctrine()->getManager();
        $clientes = $em->getRepository("WZSistemas\CobrancaBundle\Entity\Cliente")->getClientesSemCartao();
        $dependentes = $em->getRepository("WZSistemas\CobrancaBundle\Entity\Dependente")->getDepemdentesSemCartao();
        $lote = (int) $em->getRepository("WZSistemas\CobrancaBundle\Entity\Cartao")->getNumeroLote();

        $lote++;
        $cartoes = [];
        
        foreach ($clientes as $cliente) {
            $cartao = new Cartao($lote, $cliente);
            $em->persist($cartao);
            $cartoes[] = $cartao;
        }
        foreach ($dependentes as $dependente) {
            $cartao = new Cartao($lote, $dependente->getCliente(), $dependente);
            $em->persist($cartao);
            $cartoes[] = $cartao;
        }
        
        $em->flush();        
        
        $zip = new ZipArchive();
        $filename = $this->get('kernel')->getRootDir()."/../web/uploads/temp/".time().".zip";
        
        if ($zip->open($filename, ZIPARCHIVE::CREATE)!==TRUE) {
            exit("cannot open <$filename>\n");
        }
        
        $csvName = time();
        $output = fopen($this->get('kernel')->getRootDir()."/../web/uploads/temp/".$csvName.".zip", 'w');

        fputcsv($output, array('Numero', 'Nome', 'Tipo', 'Dependente'), ";");
        
        foreach ($cartoes as $cartao) {
            if ($cartao->getDependente()) {
                $file = new File($this->get('kernel')->getRootDir()."/../web/uploads/arquivos/".$cartao->getDependente()->getFoto());
            } else {
                $file = new File($this->get('kernel')->getRootDir()."/../web/uploads/arquivos/".$cartao->getCliente()->getFoto());
            }
            $numero = $cartao->getId();
            $zip->addFile($file->getPath()."/".$file->getFilename(), $numero.".".$file->getExtension());
            fputcsv($output, array($numero, $cartao->getCliente()->getNome(), $cartao->getCliente()->getTipo(), $cartao->getDependente() ? $cartao->getDependente()->getNome() : ""), ';');
        }
        fclose($output);
        $zip->addFile($this->get('kernel')->getRootDir()."/../web/uploads/temp/".$csvName.".zip", "planilha.csv");
        $zip->close();
                
        $response = new Response(file_get_contents($filename));
        $file = new File($filename);
        $response->headers->set('Content-Type', $file->getMimeType());
        $response->headers->set("Content-Disposition", "attachment; filename=cartoes.zip");
        $response->headers->set("Content-Length", $file->getSize());
        
        return $response;
    }

    
}
