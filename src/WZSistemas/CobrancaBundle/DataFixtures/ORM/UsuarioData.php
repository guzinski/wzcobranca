<?php

namespace WZSistemas\CobrancaBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use WZSistemas\CobrancaBundle\Entity\Nivel;
use WZSistemas\CobrancaBundle\Entity\Permissao;
use WZSistemas\CobrancaBundle\Entity\Usuario;

/**
 * Description of NivelData
 *
 * @author Luciano
 */
class UsuarioData implements FixtureInterface
{
    
    
    public function load(ObjectManager $manager)
    {
        $nivel = new Nivel();
        $nivel->setNome("Administrativo");
        
        $permissaoCliente = new Permissao();
        $permissaoCliente->setNome("Cliente");
        $permissaoCliente->setRole("CLIENTE");
        
        $permissaoUsuario = new Permissao();
        $permissaoUsuario->setNome("Usuários");
        $permissaoUsuario->setRole("USUARIO");
        
        $permissaoNivel = new Permissao();
        $permissaoNivel->setNome("Nível");
        $permissaoNivel->setRole("NIVEL");
        
        $permissaoConveniada = new Permissao();
        $permissaoConveniada->setNome("Convenida");
        $permissaoConveniada->setRole("CONVENIaDA");
        
        $permissaoCobranca = new Permissao();
        $permissaoCobranca->setNome("Cobrança");
        $permissaoCobranca->setRole("COBRANCA");
        
        $permissaoRelatorio = new Permissao();
        $permissaoRelatorio->setNome("Relatório");
        $permissaoRelatorio->setRole("RELATORIO");
        
        $nivel->getPermissoes()->add($permissaoUsuario);
        $nivel->getPermissoes()->add($permissaoNivel);
        $nivel->getPermissoes()->add($permissaoCobranca);
        $nivel->getPermissoes()->add($permissaoConveniada);
        $nivel->getPermissoes()->add($permissaoCliente);
        $nivel->getPermissoes()->add($permissaoRelatorio);
        
        $manager->persist($nivel);
        
        $usuario = new Usuario();
        $usuario->setEmail("luciano");
        $usuario->setNome("Luciano Guzinski");
        $usuario->setSenha("123456");
        $usuario->setNivel($nivel);
        
        $manager->persist($usuario);
        $manager->flush();
    }
    
}
