<?php

namespace WZSistemas\CobrancaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Arquivo
 *
 * @ORM\Table(name="arquivo")
 * @ORM\Entity()
 */
class Arquivo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=150, nullable=false)
     */
    private $nome;    
    
    /**
     * @var Cliente
     *
     * @ORM\ManyToOne(targetEntity="cliente", inversedBy="arquivos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cliente", referencedColumnName="id")
     * })
     */
    private $cliente;
    
    public function __construct($nome = NULL, $cliente = NULL)
    {
        if (!is_null($nome)) {
            $this->setNome($nome);
        }
        if (!is_null($cliente)) {
            $this->setCliente($cliente);
        }
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function getCliente()
    {
        return $this->cliente;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    public function setCliente(Cliente $cliente)
    {
        $this->cliente = $cliente;
        return $this;
    }

    
    
    
}
