<?php

namespace WZSistemas\CobrancaBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cliente
 *
 * @ORM\Table(name="cliente")
 * @UniqueEntity(fields={"cpf"}, message="Este CPF já está cadastrado", repositoryMethod="uniqueEntity")
 * @ORM\Entity(repositoryClass="WZSistemas\CobrancaBundle\Entity\Repository\ClienteRepository")
 */
class Cliente
{
       
    /**
     * @var string
     * @Assert\NotBlank(message="Nome deve ser preenchido")
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    private $nome;
    
    /**
     * @var string
     * @Assert\NotBlank(message="Deve ser colocado uma foto para o cliente")
     * @ORM\Column(type="string", length=150, nullable=false)
     */
    private $foto;
    
    /**
     * @var DateTime
     *
     * @ORM\Column(type="date", nullable=false)
     */
    private $nascimento;
    
    /**
     * @var string
     * @Assert\NotBlank(message="CPF deve ser PReenchido")
     * @ORM\Column(type="string", length=11, nullable=false)
     */
    private $cpf;
    
    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=12, nullable=true)
     */
    private $rg;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $cep;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    private $uf;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $cidade;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $bairro;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $endereco;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=150, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $telefone;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $telefone1;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $telefone2;
    
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $profissao;
    
    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $nit;
    
    /**
     * @var string
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $registro;
    
    
    /**
     * @var DateTime
     *
     * @ORM\Column(name="inicio_atividade", type="date", nullable=true)
     */
    private $inicioAtividade;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var Collection
     *
     * @ORM\OneToMany (targetEntity="Divida", mappedBy="cliente", cascade={"persist", "remove"})
     */
    private $dividas;

    /**
     * @var Collection
     *
     * @ORM\OneToMany (targetEntity="Registro", mappedBy="cliente", cascade={"persist", "remove"})
     */
    private $registros;
    
    /**
     * @var Collection
     *
     * @ORM\OneToMany (targetEntity="Dependente", mappedBy="cliente", cascade={"persist", "remove"})
     */
    private $dependentes;    
    
    /**
     * @var Collection
     *
     * @ORM\OneToMany (targetEntity="Cartao", mappedBy="cliente", cascade={"persist", "remove"})
     */
    private $cartoes;    
    
    /**
     * @var Collection
     * 
     * @ORM\OneToMany(targetEntity="Arquivo", mappedBy="cliente", cascade={"all"})
     **/
    private $arquivos;

    
    public function __construct()
    {
        $this->setDividas(new ArrayCollection());
        $this->setRegistros(new ArrayCollection());
        $this->setDependentes(new ArrayCollection());
        $this->setArquivos(new ArrayCollection());
        $this->setCartoes(new ArrayCollection());
    }
        
    /**
     * Set nome
     *
     * @param string $nome
     * @return Cliente
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string 
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set cpf
     *
     * @param string $cpf
     * @return Cliente
     */
    public function setCpf($cpf)
    {
        $this->cpf = preg_replace("/[^0-9]/", "", $cpf);

        return $this;
    }

    /**
     * Get cpf
     *
     * @return string 
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * Set rg
     *
     * @param string $rg
     * @return Cliente
     */
    public function setRg($rg)
    {
        $this->rg = preg_replace("/[^0-9]/", "", $rg);;

        return $this;
    }

    /**
     * Get rg
     *
     * @return string 
     */
    public function getRg()
    {
        return $this->rg;
    }

    /**
     * Set cep
     *
     * @param string $cep
     * @return Cliente
     */
    public function setCep($cep)
    {
        $this->cep = $cep;

        return $this;
    }

    /**
     * Get cep
     *
     * @return string 
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * Set uf
     *
     * @param string $uf
     * @return Cliente
     */
    public function setUf($uf)
    {
        $this->uf = $uf;

        return $this;
    }

    /**
     * Get uf
     *
     * @return string 
     */
    public function getUf()
    {
        return $this->uf;
    }

    /**
     * Set cidade
     *
     * @param string $cidade
     * @return Cliente
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;

        return $this;
    }

    /**
     * Get cidade
     *
     * @return string 
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * Set bairro
     *
     * @param string $bairro
     * @return Cliente
     */
    public function setBairro($bairro)
    {
        $this->bairro = $bairro;

        return $this;
    }

    /**
     * Get bairro
     *
     * @return string 
     */
    public function getBairro()
    {
        return $this->bairro;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Cliente
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set telefone
     *
     * @param string $telefone
     * @return Cliente
     */
    public function setTelefone($telefone)
    {
        $this->telefone = $telefone;

        return $this;
    }

    /**
     * Get telefone
     *
     * @return string 
     */
    public function getTelefone()
    {
        return $this->telefone;
    }

    /**
     * Set nascimento
     *
     * @param DateTime $nascimento
     * @return Cliente
     */
    public function setNascimento($nascimento)
    {
        $this->nascimento = $nascimento;

        return $this;
    }

    /**
     * Get nascimento
     *
     * @return DateTime 
     */
    public function getNascimento()
    {
        return $this->nascimento;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * 
     * @return string
     */
    public function getEndereco()
    {
        return $this->endereco;
    }
        
    public function getTelefone1()
    {
        return $this->telefone1;
    }

    public function getTelefone2()
    {
        return $this->telefone2;
    }

    public function getDividas()
    {
        return $this->dividas;
    }

    public function getRegistros()
    {
        return $this->registros;
    }

    public function getDependentes()
    {
        return $this->dependentes;
    }

    public function setTelefone1($telefone1)
    {
        $this->telefone1 = $telefone1;
        return $this;
    }

    public function setTelefone2($telefone2)
    {
        $this->telefone2 = $telefone2;
        return $this;
    }

    public function setDividas(Collection $dividas)
    {
        $this->dividas = $dividas;
        return $this;
    }

    public function setRegistros(Collection $registros)
    {
        $this->registros = $registros;
        return $this;
    }

    public function setDependentes(Collection $dependentes)
    {
        $this->dependentes = $dependentes;
        return $this;
    }

    public function getProfissao()
    {
        return $this->profissao;
    }

    public function getNit()
    {
        return $this->nit;
    }

    public function getRegistro()
    {
        return $this->registro;
    }

    public function setProfissao($profissao)
    {
        $this->profissao = $profissao;
        return $this;
    }

    public function setNit($nit)
    {
        $this->nit = $nit;
        return $this;
    }

    public function setRegistro($registro)
    {
        $this->registro = $registro;
        return $this;
    }
    
    public function getInicioAtividade()
    {
        return $this->inicioAtividade;
    }

    public function setInicioAtividade(DateTime $inicioAtividade = null)
    {
        $this->inicioAtividade = $inicioAtividade;
        return $this;
    }

    /**
     * 
     * @return array
     */
    public static function getTipoEnumValues()
    {
        return ['ASSOCIADO'=>'Associado', 'AMADOR'=>'Amador', 'PROFISSIONAL'=>'Profissional'];
    }

    
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
        return $this;
    }

    public function getArquivos()
    {
        return $this->arquivos;
    }

    public function setArquivos(Collection $arquivos)
    {
        $this->arquivos = $arquivos;
        return $this;
    }

    public function getFoto()
    {
        return $this->foto;
    }

    public function setFoto($foto)
    {
        $this->foto = $foto;
        return $this;
    }


    public function getCartoes()
    {
        return $this->cartoes;
    }

    public function setCartoes(Collection $cartoes)
    {
        $this->cartoes = $cartoes;
        return $this;
    }

    
    
}
