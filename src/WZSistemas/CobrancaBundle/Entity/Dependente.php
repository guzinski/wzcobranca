<?php

namespace WZSistemas\CobrancaBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Dependentes
 *
 * @ORM\Table(name="dependente")
 * @ORM\Entity(repositoryClass="WZSistemas\CobrancaBundle\Entity\Repository\DependenteRepository")
 */
class Dependente
{
    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=50, nullable=false)
     */
    private $nome;
    
    /**
     * @var string
     *
     * @Assert\NotBlank(message="Deve ser colocado uma foto para o Dependente")
     * @ORM\Column(name="foto", type="string", length=255, nullable=false)
     */
    private $foto;
    
    /**
     * @var string
     */
    private $fotoExcluida;
    
    /**
     * @var string
     *
     * @ORM\Column(name="telefone", type="string", length=20, nullable=true)
     */
    private $telefone;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="nascimento", type="date", nullable=true)
     */
    private $nascimento;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var Collection
     *
     * @ORM\OneToMany (targetEntity="Cartao", mappedBy="dependente")
     */
    private $cartoes;    

    /**
     * @var Cliente
     *
     * @ORM\ManyToOne(targetEntity="Cliente", inversedBy="dependentes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cliente", referencedColumnName="id")
     * })
     */
    private $cliente;

    public function getNome()
    {
        return $this->nome;
    }

    public function getTelefone()
    {
        return $this->telefone;
    }

    public function getNascimento()
    {
        return $this->nascimento;
    }

    public function getCliente()
    {
        return $this->cliente;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    public function setTelefone($telefone)
    {
        $this->telefone = $telefone;
        return $this;
    }

    public function setNascimento(DateTime $nascimento)
    {
        $this->nascimento = $nascimento;
        return $this;
    }

    public function setCliente(Cliente $cliente)
    {
        $this->cliente = $cliente;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    

    public function getFoto()
    {
        return $this->foto;
    }

    public function setFoto($foto)
    {
        $this->foto = $foto;
        return $this;
    }



    public function getFotoExcluida()
    {
        return $this->fotoExcluida;
    }

    public function setFotoExcluida($fotoExcluida)
    {
        $this->fotoExcluida = $fotoExcluida;
        return $this;
    }




}
