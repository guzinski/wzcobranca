<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace WZSistemas\CobrancaBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of CartaoRepository
 *
 */
class CartaoRepository extends EntityRepository
{

    
    public function getNumeroLote()
    {
        $query = $this->createQueryBuilder("C");
        $query->select($query->expr()->max("C.lote"));
             
             
        return $query->getQuery()->getSingleScalarResult();;
    }
    
    
}
