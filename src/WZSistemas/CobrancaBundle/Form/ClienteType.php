<?php

namespace WZSistemas\CobrancaBundle\Form;

use WZSistemas\CobrancaBundle\Entity\Cliente;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of ClienteType
 *
 * @author Luciano
 */
class ClienteType extends AbstractType
{
    
    public function getName()
    {
        return "cliente";
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $em = $options['em'];
        $builder->add("nome")
                ->add("foto", "hidden")
                ->add("nascimento", "date", array(
                            'label'  => 'Data de Nascimento',
                            'widget' => 'single_text',
                            'format' => 'dd/MM/yyyy',
                  ))
                ->add("cpf", "text", ['label'=>"CPF"])
                ->add("rg", "text", ['label'=>"RG", 'required'=>FALSE])
                ->add("cep", "text", ['label'=>"CEP", 'required'=>FALSE])
                ->add("uf", "text", ['required'=>FALSE, 'attr'=>['readOnly'=>'disabled']])
                ->add("cidade", "text", ['attr'=>['readOnly'=>'disabled'], 'required'=>FALSE])
                ->add("bairro")
                ->add("endereco", "text", ['label'=>"Endereço"])
                ->add("email", 'email', ['label'=>'E-mail', 'required'=>FALSE])
                ->add("telefone")
                ->add("telefone1", "text", ['label'=>'Telefone 2', 'required'=>FALSE])
                ->add("telefone2", "text", ['label'=>'Telefone 3', 'required'=>FALSE])
                ->add("profissao", "text", ['label'=>"Profissão", 'required'=>FALSE])
                ->add("registro", "text", ['label'=>"Regisro RGP", 'required'=>FALSE])
                ->add("nit", "text", ['label'=>'NIT', 'required'=>FALSE])
                ->add("inicioAtividade", "date", array(
                            'label'  => 'Início da Atividade',
                            'widget' => 'single_text',
                            'format' => 'dd/MM/yyyy',
                            'required'=>FALSE,
                  ))
                ->add($builder->create("arquivos", "collection", array(
                        'type'          => 'hidden',
                        'allow_add'     => true,
                        'allow_delete'  => true,
                        'label'         => false,
                        'data_class'    => null
                ))->addModelTransformer(new ArquivoTransformer($em)))
                ->add("dividas", "collection", array(
                        'type'          => new DividaType(),
                        'allow_add'     => true,
                        'allow_delete'  => true,
                        'label'         => false,
                ))
                ->add("dependentes", "collection", array(
                        'type'          => new DependenteType(),
                        'allow_add'     => true,
                        'allow_delete'  => true,
                        'label'         => false,
                        'cascade_validation' => true,

                ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) 
    {
        $resolver->setDefaults(['data_class' => 'WZSistemas\CobrancaBundle\Entity\Cliente', 'cascade_validation' => true])
                ->setRequired(['em'])
                ->setAllowedTypes(['em' => 'Doctrine\Common\Persistence\ObjectManager']);
    }

    
    
}
