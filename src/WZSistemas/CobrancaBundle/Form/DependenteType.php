<?php

namespace WZSistemas\CobrancaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of DependenteType
 *
 * @author Luciano
 */
class DependenteType extends AbstractType
{
    public function getName()
    {
        return 'dependente';
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add("nome", "text", ['attr'=>['placeholder'=>'Nome']])
                ->add("telefone", "text", ["required"=>false, 'attr'=>['placeholder'=>'Telefone']])
                ->add("foto", "hidden")
                ->add("fotoExcluida", "hidden")
                ->add("nascimento", "date", [
                            'label'  => 'Data de Nascimento',
                            'widget' => 'single_text',
                            'format' => 'dd/MM/yyyy',
                            'attr'=>['placeholder'=>'Nascimento']
                ]);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'    => 'WZSistemas\CobrancaBundle\Entity\Dependente',
            'label'         => false
        ));
    }

    
}
