<?php

namespace WZSistemas\CobrancaBundle\Service;

use AppKernel;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use WZSistemas\CobrancaBundle\Entity\Dependente;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Description of Cliente
 *
 * @author Luciano
 */
class Cliente
{
    /**
     *
     * @var RequestStack 
     */
    protected $requestStack;
    /**
     *
     * @var AppKernel 
     */
    protected $kernel;
    /**
     *
     * @var Upload 
     */
    protected $upload;
    /**
     *
     * @var EntityManager 
     */
    protected $em;

    public function __construct(RequestStack $requestStack, AppKernel $kernel, Upload $upload,  EntityManager $em)
    {
        $this->requestStack = $requestStack;
        $this->kernel = $kernel;
        $this->upload = $upload;
        $this->em = $em;
    }

    /**
     * @param \WZSistemas\CobrancaBundle\Entity\Cliente $cliente
     */
    public function salvarCliente(\WZSistemas\CobrancaBundle\Entity\Cliente $cliente, Collection $arquivos, Collection $dependentes)
    {
        $request = $this->requestStack->getCurrentRequest();
        
        $fotoExcluida   = $request->get("fotoExcluida", "");
        $pathTemp       = $this->kernel->getRootDir()."/../web/uploads/temp/";
        $path           = $this->kernel->getRootDir()."/../web/uploads/arquivos/";

        $this->salvarFoto($cliente, $fotoExcluida);
        
        foreach ($cliente->getArquivos() as $arquivo) {
            if (file_exists($pathTemp.$arquivo->getNome())) {
                $originalName = $arquivo->getNome();
                $arquivo->setNome($this->upload->getRealName($arquivo->getNome(), $path));
                $arquivo->setCliente($cliente);
                rename($pathTemp.$originalName, $path.$arquivo->getNome());
            }
        }
        foreach ($arquivos as $arquivo) {
            if (!$cliente->getArquivos()->contains($arquivo)) {
                if (file_exists($path.$arquivo->getNome())) {
                    unlink($path.$arquivo->getNome());
                }
                $this->em->remove($arquivo);
            }
        }
        foreach ($cliente->getDividas() as $divida) {
            $cliente->getDividas()->add($divida->setCliente($cliente));
        }
        foreach ($cliente->getDependentes() as $dependente) {
            $this->salvarFotoDependente($dependente, "a");
            $cliente->getDependentes()->add($dependente->setCliente($cliente));
        }
        
        foreach ($dependentes as $dependente) {
            if (!$cliente->getDependentes()->contains($dependente)) {
                if (file_exists($path.$dependente->getFoto())) {
                    unlink($path.$dependente->getFoto());
                }
                $this->em->remove($dependente);
            }
        }
        
        $this->em->persist($cliente);
        $this->em->flush();
    }
    
    
    
    private function salvarFoto(\WZSistemas\CobrancaBundle\Entity\Cliente $cliente, $fotoExcluida)
    {
        $pathTemp       = $this->kernel->getRootDir()."/../web/uploads/temp/";
        $path           = $this->kernel->getRootDir()."/../web/uploads/arquivos/";
        if (!empty($fotoExcluida) && $cliente->getId()>0 && file_exists($path.$fotoExcluida)) {
            unlink($path.$fotoExcluida);
        }

        if (file_exists($pathTemp.$cliente->getFoto())) {
            $originalNameFoto = $cliente->getFoto();
            $cliente->setFoto($this->upload->getRealName($cliente->getFoto(), $path));
            rename($pathTemp.$originalNameFoto, $path.$cliente->getFoto());
        }
    }
    
    private function salvarFotoDependente(Dependente $dependente)
    {
        $pathTemp       = $this->kernel->getRootDir()."/../web/uploads/temp/";
        $path           = $this->kernel->getRootDir()."/../web/uploads/arquivos/";
        $fotoExcluida   = $dependente->getFotoExcluida();
        if (!empty($fotoExcluida) && $dependente->getId()>0 && file_exists($path.$fotoExcluida)) {
            unlink($path.$fotoExcluida);
        }

        if (file_exists($pathTemp.$dependente->getFoto())) {
            $originalNameFoto = $dependente->getFoto();
            $dependente->setFoto($this->upload->getRealName($dependente->getFoto(), $path));
            rename($pathTemp.$originalNameFoto, $path.$dependente->getFoto());
        }
    }

    
}
